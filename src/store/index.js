import { createStore } from "vuex";
import ApiService from "../services/api.service";

export default createStore({
  state: {
    movies: [],
  },
  mutations: {
    setProducts(state, payload) {
      state.movies = payload;
    },
  },
  actions: {
    async getMovies({ commit }) {},
  },
  modules: {},
});
