import axios from "axios";

const API_KEY = "9fb98527ed4b2f39026a8ba4ff0e1683";

const BASE_URL = "https://api.themoviedb.org/3/movie/";

class ApiService {
  constructor() {
    this.resource = axios.create({ baseURL: BASE_URL });
    this.apiKey = `api_key=${API_KEY}&language=en-US`;
  }

  getPopular = (currentPage) =>
    this.resource.get(`popular?${this.apiKey}&page=${currentPage}`);

  getTopRated = (currentPage) =>
    this.resource.get(`top_rated?${this.apiKey}&page=${currentPage}`);

  getMovie = (movieId) => this.resource.get(`${movieId}?${this.apiKey}`);
}

export default new ApiService();
